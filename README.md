# 有爱编辑器 

#### ver 2.1 [更新说明](http://uiwow.com/thread-10101-1-1.html)
编辑器框架侧更新:

* 修改多个编辑器之间的切换的方式
* 增加编辑器标签可拖出的建立子窗口的功能
* 增加窗口嵌入到其他窗口的功能
* 修改字段跳转功能的样式和处理方法(目前写了item_template表作为体验)
* 把各个编辑器sql执行时的语句展示移动到各个编辑器中
* 修复若干测试时发现的bug
* 增加lua侧的api`OnValueChange`(编辑页的字段值修改时),可在字段定义时绑定一个function方法,在字段值更改时会执行
* 增加lua侧的api`OnFilterValueChange`(筛选页的字段值修改时),和上面类似,在筛选页的字段值改变时执行

lua侧更新:

* 删除原来的item_template.lua(单文件版)并重写了个新的,新的那个需要使用mysql中的表格定义
* 数据库表格增加新表:`_editors_field_flags`,用于记录某些字段flag类型的选项定义
* 数据库增加新表:`_editors_field_selects`,用于记录选择型的字段的选项项目
* 数据库uieditors增加了所有的335dbc的数据表.
* 修复若干bug

版本号跳了几个才发,之前还有1.8 1.9等几个,不过因每次更新都要写次帖子

然后拖延症弄着弄着就2.1了
这个版本基本大部分字段定义都放到mysql的表格中定义,
以前的单个lua文件全部定义完的形式也是兼容的,不过这个方式会有挺多的重复代码
所以改了一下,布局和字段的定义分开了
这是item_template的 `属性`的布局定义,相对以前的写在一起

```
{
        borderColor = "#CCCCCC",
        "StatsCount",
        {
            borderColor = "#CCCCCC",orientation = true,
            {"stat_type1","stat_type2","stat_type3","stat_type4","stat_type5","stat_type6","stat_type7","stat_type8","stat_type9","stat_type10",},
            {"stat_value1","stat_value2","stat_value3","stat_value4","stat_value5","stat_value6","stat_value7","stat_value8","stat_value9","stat_value10",},
        },
        {orientation = true,borderColor = "#CCCCCC","ScalingStatDistribution", "ScalingStatValue"}
    },
```

现在是通过字段的名字,然后使用字段定义替换掉所在的位置
其实两种方式最终的结果是一样的,只是以前只能全写在一起(旧的 `item_template.lua `)
而现在有另一个选择(新的 `item_template.lua`)

-----


1.7.x以下的可以查看之前写的帖子
#### ver 1.0 测试版 [lua编写说明](http://uiwow.com/forum.php?mod=redirect&goto=findpost&ptid=8770&pid=233262)

#### ver 1.6 [相关说明](http://uiwow.com/thread-9232-1-1.html)

-------
这是一个使用lua作为配置文件的mysql数据库可视化编辑器.

例如编辑表`_editors`:
![_editors表](https://images.gitee.com/uploads/images/2020/1126/220929_15d6d632_72264.png "屏幕截图.png")
新建lua文件,写入如下内容即可使用.
```
SetEditor("uieditors","_editors",{
    Name = "有爱编辑器表格设计器",
    Fields = {
        {field = "ID", tooltip = "编辑器id,用于标识编辑器,唯一,可与_editors_field表对应", name = "编辑器ID",isFilter = true, },
        {field = "Database", tooltip = "编辑器的所在数据库", name = "数据库名",isFilter = true, },
        {field = "Table", tooltip = "需要编辑的表格名", name = "表格名",isFilter = true, },
        {field = "Name", tooltip = "编辑器的名称", name = "编辑器名称",isFilter = true, },
        {field = "MysqlHost", tooltip = "数据库的地址或ip\n如果表格和setting.lua中所设置的数据库不是同一个的话,请设置这里的相关参数连接外部数据库",isFilter = true, },
        {field = "MysqlPort", tooltip = "数据库端口\n如果表格和setting.lua中所设置的数据库不是同一个的话,请设置这里的相关参数连接外部数据库",isFilter = true, },
        {field = "MysqlUser", tooltip = "数据库用户名\n如果表格和setting.lua中所设置的数据库不是同一个的话,请设置这里的相关参数连接外部数据库",isFilter = true, },
        {field = "MysqlPassword", tooltip = "数据库密码\n如果表格和setting.lua中所设置的数据库不是同一个的话,请设置这里的相关参数连接外部数据库",},
    }
})
```
![预览](https://images.gitee.com/uploads/images/2020/1126/220741_b98d8aea_72264.gif "a1.gif")